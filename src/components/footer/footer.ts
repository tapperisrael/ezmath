import {Component, Input} from '@angular/core';
import {NavController} from "ionic-angular";
import {PushnotificationsPage} from "../../pages/pushnotifications/pushnotifications";

/**
 * Generated class for the FooterComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'footer',
  templateUrl: 'footer.html'
})
export class FooterComponent {


@Input() messagesCount;

    constructor(public navCtrl: NavController) {
  }


    goHomePage() {
        this.navCtrl.setRoot('MainPage');
    }

    goPreviousClassesPage() {
        this.navCtrl.push('PastclassesPage');
    }

    goProfilePage() {
        this.navCtrl.push('ProfileeditPage');
    }

    goFutureClassesPage() {
        this.navCtrl.push('FutureclassesPage');
    }

    goPushNotifications() {
        this.navCtrl.push(PushnotificationsPage);
    }
}
