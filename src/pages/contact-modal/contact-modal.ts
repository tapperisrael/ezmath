import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {ServerService} from "../../services/server-service";
import {ToastService} from "../../services/toast-service";

/**
 * Generated class for the ContactModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contact-modal',
  templateUrl: 'contact-modal.html',
})
export class ContactModalPage {

    public info: any = {
        fullname: '',
        phone: '',
        description: '',
    };

  constructor(public navCtrl: NavController, public navParams: NavParams,public server:ServerService,public Toast:ToastService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactModalPage');
  }

    sendContact() {

        if (!this.info.fullname)
          this.Toast.presentToast('הכנס שם מלא');
        else if (!this.info.phone)
            this.Toast.presentToast('הכנס טלפון');

        else if (this.info['phone'].length < 8 || this.info['phone'].length > 10)
            this.Toast.presentToast('הכנס מספר טלפון חוקי');

        else if (!this.info.description)
            this.Toast.presentToast('הכנס תיאור הפנייה');

        else {

            this.server.ContactSignup('ContactSignup',this.info,localStorage.getItem("userid")).then((data: any) => {
                this.info.fullname = '';
                this.info.phone  = '';
                this.info.description = '';
                this.closeModal();
            });
        }
    }

    closeModal() {
        this.navCtrl.pop();
    }

}
