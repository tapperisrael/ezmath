import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PastclassesPage } from './pastclasses';
import {ComponentsModule} from '../../components/components.module';

@NgModule({
  declarations: [
    PastclassesPage,
  ],
  imports: [
    IonicPageModule.forChild(PastclassesPage),
      ComponentsModule,
  ],
})
export class PastclassesPageModule {}
