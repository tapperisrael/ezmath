import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ServerService} from "../../services/server-service";

/**
 * Generated class for the PastclassesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pastclasses',
  templateUrl: 'pastclasses.html',
})
export class PastclassesPage implements OnInit {


    public ClassesArray:any = [];
    public ClassesArray2:any = [];
  public ClassStatus :any =  ["חדש","מאושר","לא מאושר"];
  public searchInput:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public server:ServerService) {
  }

    async ngOnInit() {
        this.getPastClasses();
    }


    getPastClasses()
    {
        this.server.getPastClasses('getPastClasses',localStorage.getItem("userid")).then((data: any) => {
            console.log("getPastClasses : " , data);
            let response = data.json();
            this.ClassesArray = response;
            this.ClassesArray2 = response;
            console.log("ClassesArray",this.ClassesArray)
        });
    }

    getClassStatus(status) {
      return this.ClassStatus[status];
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PastclassesPage');
  }


    filterItems(ev: any) {
        let val = ev.target.value;

        // if the value is an empty string don't filter the items
        if (val && val.trim() != '' ) {
            this.ClassesArray = this.ClassesArray.filter((item) => {
                return (item.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        }


    }

    onCancel(ev: any) {
        this.ClassesArray = this.ClassesArray2;
        //this.getPastClasses();
    }

}
