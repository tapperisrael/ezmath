import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PreviousticketsPage } from './previoustickets';
import {ComponentsModule} from '../../components/components.module';

@NgModule({
  declarations: [
    PreviousticketsPage,
  ],
  imports: [
    IonicPageModule.forChild(PreviousticketsPage),
      ComponentsModule
  ],
})
export class PreviousticketsPageModule {}
