import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ViewController} from 'ionic-angular';
import {ServerService} from "../../services/server-service";
import {ToastService} from "../../services/toast-service";

/**
 * Generated class for the ClasssignupmodalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-classsignupmodal',
  templateUrl: 'classsignupmodal.html',
})
export class ClasssignupmodalPage {

  public data:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,public server: ServerService,public Toast:ToastService) {
     this.data = this.navParams.get('details');
     console.log("data:" , this.data)

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClasssignupmodalPage');
  }

    confirmSignup() {
        this.server.registerUserToClass('ConfirmregisterUserToClass',localStorage.getItem("userid"),this.data.id).then((data: any) => {
            console.log("ConfirmregisterUserToClass : " , data.json());
            let response = data.json();
            if (response.max_quan_reached == 0) {
                this.data.already_signup = 1;
                this.closeModal();
                this.Toast.presentToast('הרשמה לשיעור בוצעה בהצלחה');
            }
            else {
                this.Toast.presentToast('לא ניתן להרשם לשיעור זה, מכסת תלמידים לשיעור הסתיים');
            }
        });
    }


    closeModal() {
        this.viewCtrl.dismiss(this.data);
    }

}
