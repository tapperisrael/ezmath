import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClasssignupmodalPage } from './classsignupmodal';

@NgModule({
  declarations: [
    ClasssignupmodalPage,
  ],
  imports: [
    IonicPageModule.forChild(ClasssignupmodalPage),
  ],
})
export class ClasssignupmodalPageModule {}
