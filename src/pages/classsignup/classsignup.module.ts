import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClasssignupPage } from './classsignup';

@NgModule({
  declarations: [
    ClasssignupPage,
  ],
  imports: [
    IonicPageModule.forChild(ClasssignupPage),
  ],
})
export class ClasssignupPageModule {}
