import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfileeditPage } from './profileedit';
import {ComponentsModule} from '../../components/components.module';

@NgModule({
  declarations: [
    ProfileeditPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfileeditPage),
      ComponentsModule,
  ],
})
export class ProfileeditPageModule {}
