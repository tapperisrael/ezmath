import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FormControl, FormGroup, Validators,NgForm} from "@angular/forms";
import {ServerService} from "../../services/server-service";

/**
 * Generated class for the ProfileeditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profileedit',
  templateUrl: 'profileedit.html',
})
export class ProfileeditPage implements OnInit {

  public userProfileArray:any = [];
  public userfields:any = [];
  public waitforsave:boolean = false;
    public schoolgradeArray : any = [];
    public teachinglevelArray : any = [];
    public branchesArray : any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public server: ServerService) {
  }



    async getSchoolGrade() {

        await this.server.GetData('webGetSchoolGrade').then((data: any) => {
            this.schoolgradeArray = data.json();
            console.log("webGetSchoolGrade : " , data.json());
        });
    }

    async getTeachingLevel() {
        await this.server.GetData('webGetTeachingLevel').then((data: any) => {
            console.log("webGetTeachingLevel : " , data.json());
            this.teachinglevelArray = data.json();
        });
    }

    async getBranches() {
        await this.server.GetData('GetBranches').then((data: any) => {
            console.log("GetBranches : " , data.json());
            this.branchesArray = data.json();
        });
    }



    getStudentData()
  {
      this.server.getStudentData('getStudentData',localStorage.getItem("userid")).then((data: any) => {
          console.log("getStudentData : " , data);
          let response = data.json();
          this.userProfileArray = response;
          this.waitforsave = true;
          console.log("getStudentData : " , response);
      });
  }


  async ngOnInit() {
      this.getSchoolGrade();
      this.getTeachingLevel();
    this.getStudentData();
    this.getBranches();
  }

    onSubmit(form:NgForm)
    {
        console.log(form.value);

        if (this.waitforsave)
        {
            this.server.saveUserProfile('saveUserProfile',form.value,localStorage.getItem("userid")).then((data: any) => {
                console.log("saveUserProfile : " , data);
                this.navCtrl.setRoot('MainPage');
            });
        }

    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfileeditPage');
  }

}
