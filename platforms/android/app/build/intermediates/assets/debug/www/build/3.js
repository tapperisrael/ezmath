webpackJsonp([3],{

/***/ 834:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileeditPageModule", function() { return ProfileeditPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profileedit__ = __webpack_require__(844);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(427);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ProfileeditPageModule = /** @class */ (function () {
    function ProfileeditPageModule() {
    }
    ProfileeditPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__profileedit__["a" /* ProfileeditPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__profileedit__["a" /* ProfileeditPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
            ],
        })
    ], ProfileeditPageModule);
    return ProfileeditPageModule;
}());

//# sourceMappingURL=profileedit.module.js.map

/***/ }),

/***/ 844:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileeditPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_server_service__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



/**
 * Generated class for the ProfileeditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProfileeditPage = /** @class */ (function () {
    function ProfileeditPage(navCtrl, navParams, server) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.server = server;
        this.userProfileArray = [];
        this.userfields = [];
        this.waitforsave = false;
        this.schoolgradeArray = [];
        this.teachinglevelArray = [];
        this.branchesArray = [];
    }
    ProfileeditPage.prototype.getSchoolGrade = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.server.GetData('webGetSchoolGrade').then(function (data) {
                            _this.schoolgradeArray = data.json();
                            console.log("webGetSchoolGrade : ", data.json());
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfileeditPage.prototype.getTeachingLevel = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.server.GetData('webGetTeachingLevel').then(function (data) {
                            console.log("webGetTeachingLevel : ", data.json());
                            _this.teachinglevelArray = data.json();
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfileeditPage.prototype.getBranches = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.server.GetData('GetBranches').then(function (data) {
                            console.log("GetBranches : ", data.json());
                            _this.branchesArray = data.json();
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfileeditPage.prototype.getStudentData = function () {
        var _this = this;
        this.server.getStudentData('getStudentData', localStorage.getItem("userid")).then(function (data) {
            console.log("getStudentData : ", data);
            var response = data.json();
            _this.userProfileArray = response;
            _this.waitforsave = true;
            console.log("getStudentData : ", response);
        });
    };
    ProfileeditPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.getSchoolGrade();
                this.getTeachingLevel();
                this.getStudentData();
                this.getBranches();
                return [2 /*return*/];
            });
        });
    };
    ProfileeditPage.prototype.onSubmit = function (form) {
        var _this = this;
        console.log(form.value);
        if (this.waitforsave) {
            this.server.saveUserProfile('saveUserProfile', form.value, localStorage.getItem("userid")).then(function (data) {
                console.log("saveUserProfile : ", data);
                _this.navCtrl.setRoot('MainPage');
            });
        }
    };
    ProfileeditPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfileeditPage');
    };
    ProfileeditPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profileedit',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\ezmath\src\pages\profileedit\profileedit.html"*/'<!--<ion-header>-->\n\n\n\n  <!--<ion-navbar>-->\n\n    <!--<ion-title>פרטי תלמיד</ion-title>-->\n\n  <!--</ion-navbar>-->\n\n\n\n<!--</ion-header>-->\n\n\n\n<ion-content>\n\n  <back-button></back-button>\n\n\n\n  <div class="mainLogo" align="center">\n\n    <img src="images/logo.png" style="width:50%; margin: auto">\n\n  </div>\n\n  <div class="info" align="center">\n\n    <div class="title"></div>\n\n    <form (ngSubmit)="onSubmit(f)" #f="ngForm">\n\n\n\n      <div class="form-group">\n\n        <input type="text" class="form-control textWhite"  name="student_name" [(ngModel)]="userProfileArray.student_name" required  placeholder="הכנס שם תלמיד"><br>\n\n      </div>\n\n\n\n      <div class="form-group">\n\n        <input type="tel" class="form-control textWhite"  name="phone" [(ngModel)]="userProfileArray.phone" required  placeholder="הכנס טלפון"><br>\n\n      </div>\n\n\n\n\n\n      <div class="form-group">\n\n        <input type="text" class="form-control textWhite"  name="city" [(ngModel)]="userProfileArray.city" required  placeholder="הכנס עיר"><br>\n\n      </div>\n\n\n\n      <div class="form-group">\n\n\n\n        <select id="schoolgrade" class="form-control"  name="schoolgrade" [(ngModel)]="userProfileArray.schoolgrade" required >\n\n          <option value="" selected>בחירת כיתה</option>\n\n          <option *ngFor="let item of schoolgradeArray let i=index" [value]="item.id" >{{item.title}}</option>\n\n\n\n        </select>\n\n      </div>\n\n\n\n      <div class="form-group" >\n\n        <select id="teachinglevel" class="form-control"  name="teachinglevel" [(ngModel)]="userProfileArray.teachinglevel" required >\n\n          <option value="" selected>בחירת רמת לימוד</option>\n\n          <option *ngFor="let item of teachinglevelArray let i=index" [value]="item.id" >{{item.title}}</option>\n\n        </select>\n\n      </div>\n\n\n\n      <div class="form-group" >\n\n        <select id="branch_id" class="form-control"  name="branch_id" [(ngModel)]="userProfileArray.branch_id" required >\n\n          <option value="" selected>בחירת סניף</option>\n\n          <option *ngFor="let item of branchesArray let i=index" [value]="item.id" >{{item.title}}</option>\n\n        </select>\n\n      </div>\n\n\n\n      <div class="form-group">\n\n        <input type="text" class="form-control textWhite"  name="scool_name" [(ngModel)]="userProfileArray.scool_name" required  placeholder="* הכנס שם בית ספר"><br>\n\n      </div>\n\n\n\n      <div class="form-group">\n\n        <input type="text" class="form-control textWhite"  name="parent_name" [(ngModel)]="userProfileArray.parent_name" required  placeholder="* הכנס שם הורה"><br>\n\n      </div>\n\n\n\n      <div class="form-group">\n\n        <input type="tel" class="form-control textWhite"  name="parent_phone" [(ngModel)]="userProfileArray.parent_phone" required  placeholder="* הכנס טלפון הורה"><br>\n\n      </div>\n\n\n\n\n\n      <button ion-button class="regButton" color="secondary" [disabled]="!f.valid" type="submit">עדכן</button>\n\n    </form>\n\n  </div>\n\n\n\n</ion-content>\n\n\n\n<ion-footer>\n\n  <footer></footer>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Desktop\github\ezmath\src\pages\profileedit\profileedit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_server_service__["a" /* ServerService */]])
    ], ProfileeditPage);
    return ProfileeditPage;
}());

//# sourceMappingURL=profileedit.js.map

/***/ })

});
//# sourceMappingURL=3.js.map