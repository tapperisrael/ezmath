webpackJsonp([2],{

/***/ 835:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterIntroPageModule", function() { return RegisterIntroPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__register_intro__ = __webpack_require__(845);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(427);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var RegisterIntroPageModule = /** @class */ (function () {
    function RegisterIntroPageModule() {
    }
    RegisterIntroPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__register_intro__["a" /* RegisterIntroPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__register_intro__["a" /* RegisterIntroPage */]), __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
            ],
        })
    ], RegisterIntroPageModule);
    return RegisterIntroPageModule;
}());

//# sourceMappingURL=register-intro.module.js.map

/***/ }),

/***/ 845:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterIntroPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_facebook__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_auth_service__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_toast_service__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the RegisterIntroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegisterIntroPage = /** @class */ (function () {
    function RegisterIntroPage(navCtrl, navParams, fb, auth, toast) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        this.auth = auth;
        this.toast = toast;
        this.isLoggedIn = false;
    }
    RegisterIntroPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegisterIntroPage');
        //this.fb.browserInit(AppSettings.FACEBOOKID, "v2.8");
    };
    RegisterIntroPage.prototype.goLogin = function () {
        this.navCtrl.push('LoginPage');
    };
    RegisterIntroPage.prototype.goToRegister = function (type) {
        this.navCtrl.push('RegisterPage', { type: type });
    };
    RegisterIntroPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-register-intro',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\ezmath\src\pages\register-intro\register-intro.html"*/'\n\n\n\n<ion-content>\n\n    <div class="mainLogo" align="center">\n\n        <img src="images/logo.png" style="width:50%; margin: auto">\n\n    </div>\n\n    <div class="mainTitle"> הרשמה / התחברות</div>\n\n    <div class="info" align="center">\n\n        <div class="client" (click)="goLogin()">\n\n            <img src="images/main_bt1.png" style="width: 100%" />\n\n        </div>\n\n        <div class="client MT20" (click)="goToRegister(1)" >\n\n            <img src="images/main_bt2.png" style="width: 100%" class="MT20" />\n\n        </div>\n\n\n\n        <div class="client" >\n\n            <facebookconnect></facebookconnect>\n\n        </div>\n\n    </div>\n\n\n\n\n\n\n\n\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\github\ezmath\src\pages\register-intro\register-intro.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_facebook__["a" /* Facebook */], __WEBPACK_IMPORTED_MODULE_3__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_4__services_toast_service__["a" /* ToastService */]])
    ], RegisterIntroPage);
    return RegisterIntroPage;
}());

//# sourceMappingURL=register-intro.js.map

/***/ })

});
//# sourceMappingURL=2.js.map