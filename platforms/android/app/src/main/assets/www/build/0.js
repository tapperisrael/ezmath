webpackJsonp([0],{

/***/ 837:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeacherconfirmclassPageModule", function() { return TeacherconfirmclassPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__teacherconfirmclass__ = __webpack_require__(846);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TeacherconfirmclassPageModule = /** @class */ (function () {
    function TeacherconfirmclassPageModule() {
    }
    TeacherconfirmclassPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__teacherconfirmclass__["a" /* TeacherconfirmclassPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__teacherconfirmclass__["a" /* TeacherconfirmclassPage */]),
            ],
        })
    ], TeacherconfirmclassPageModule);
    return TeacherconfirmclassPageModule;
}());

//# sourceMappingURL=teacherconfirmclass.module.js.map

/***/ }),

/***/ 846:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TeacherconfirmclassPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_server_service__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_toast_service__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





/**
 * Generated class for the TeacherconfirmclassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TeacherconfirmclassPage = /** @class */ (function () {
    function TeacherconfirmclassPage(navCtrl, navParams, server, Toast, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.server = server;
        this.Toast = Toast;
        this.alertCtrl = alertCtrl;
        this.todaydate = __WEBPACK_IMPORTED_MODULE_2_moment___default()().format('YYYY-MM-DD');
        this.info = {
            'branchselect': '',
            'professionselect': '',
            'class_date': this.todaydate,
        };
        this.senddetails = {
            'user_id': '',
            'class_id': '',
            'approve_status': '',
            'payment_method': '',
        };
        this.searchArray = [];
        this.showFieldsdiv = true;
    }
    TeacherconfirmclassPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TeacherconfirmclassPage');
    };
    TeacherconfirmclassPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.getBranches();
                return [2 /*return*/];
            });
        });
    };
    TeacherconfirmclassPage.prototype.getBranches = function () {
        var _this = this;
        this.server.GetBranches('GetBranches').then(function (data) {
            console.log("GetBranches : ", data.json());
            _this.brancesArray = data.json();
        });
    };
    TeacherconfirmclassPage.prototype.GetProfessions = function () {
        var _this = this;
        this.server.GetBranches('GetProfessions').then(function (data) {
            console.log("GetProfessions : ", data.json());
            _this.professionArray = data.json();
        });
    };
    TeacherconfirmclassPage.prototype.confirmClass = function (row, type) {
        this.senddetails.user_id = row.user_id;
        this.senddetails.class_id = row.class_id;
        this.senddetails.approve_status = type;
        this.server.SearchClasses('ApproveClassesManager', this.senddetails, localStorage.getItem("userid")).then(function (data) {
            console.log("ApproveClassesManager : ", data.json());
            row.confirmed_status = type;
        });
    };
    TeacherconfirmclassPage.prototype.didntShowClass = function (row, type) {
        this.senddetails.user_id = row.user_id;
        this.senddetails.class_id = row.class_id;
        this.server.userDidntShow('userDidntShow', this.senddetails, localStorage.getItem("userid")).then(function (data) {
            console.log("userDidntShow : ", data.json());
            var dataReponse = data.json();
            var alertResponse = '';
            if (type == 0)
                row.didnt_show = 1;
            else
                row.didnt_show = 0;
        });
    };
    TeacherconfirmclassPage.prototype.searchClass = function () {
        // if (!this.info.professionselect)
        //   this.Toast.presentToast('יש לבחור מקצוע');
        var _this = this;
        if (!this.info.branchselect)
            this.Toast.presentToast('יש לבחור סניף');
        else if (!this.info.class_date)
            this.Toast.presentToast('יש לבחור תאריך');
        else {
            this.server.SearchClasses('SearchClassesManager', this.info, localStorage.getItem("userid")).then(function (data) {
                console.log("SearchClassesManager1111 : ", data.json());
                _this.searchArray = data.json();
                if (data.json() == 0) {
                    _this.Toast.presentToast('לא נמצאו תוצאות');
                    _this.searchArray = [];
                }
                else {
                    _this.showFieldsdiv = false;
                }
            });
        }
    };
    TeacherconfirmclassPage.prototype.showFieldsSearch = function () {
        this.showFieldsdiv = true;
    };
    TeacherconfirmclassPage.prototype.paymentCash = function (row) {
        var _this = this;
        this.senddetails.user_id = row.user_id;
        this.senddetails.class_id = row.class_id;
        this.senddetails.payment_method = 0;
        var alertConfirm = this.alertCtrl.create({
            title: 'אישור רכישה',
            message: 'האם לאשר רכישה במזומן?',
            buttons: [
                {
                    text: 'אישור',
                    handler: function () {
                        _this.server.ClassPayment('ClassCashPayment', _this.senddetails, localStorage.getItem("userid")).then(function (data) {
                            console.log("ClassCashPayment : ", data.json());
                            var dataReponse = data.json();
                            var alertResponse = '';
                            switch (dataReponse) {
                                case 0:
                                    alertResponse = 'רכישה במזומן בוצעה בהצלחה';
                                    break;
                                case 1:
                                    alertResponse = 'תשלום כבר בוצעה';
                                    break;
                                default:
                                    alertResponse = '';
                            }
                            var okAlert = _this.alertCtrl.create({
                                title: alertResponse,
                                buttons: ['אישור']
                            });
                            okAlert.present();
                            row.paid_count = 1;
                            row.paid_method = 0;
                        });
                    }
                },
                {
                    text: 'ביטול',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        alertConfirm.present();
    };
    TeacherconfirmclassPage.prototype.paymentTicket = function (row) {
        var _this = this;
        this.senddetails.user_id = row.user_id;
        this.senddetails.class_id = row.class_id;
        this.senddetails.payment_method = 1;
        var alertConfirm = this.alertCtrl.create({
            title: 'אישור רכישה',
            message: 'האם לאשר רכישה בכרטיסיה?',
            buttons: [
                {
                    text: 'אישור',
                    handler: function () {
                        _this.server.ClassPayment('ClassTicketPaymentConfirm', _this.senddetails, localStorage.getItem("userid")).then(function (data) {
                            console.log("ClassTicketPaymentConfirm : ", data.json());
                            var dataReponse = data.json();
                            var alertResponse = '';
                            switch (dataReponse) {
                                case 0:
                                    alertResponse = 'רכישה בכרטיסיה בוצעה בהצלחה';
                                    break;
                                case 1:
                                    alertResponse = 'תשלום כבר בוצעה';
                                    break;
                                case 2:
                                    alertResponse = 'למשתמש זה לא שויך קבוצת כרטסיות , יש לפתוח קבוצה במערכת ניהול';
                                    break;
                                case 3:
                                    alertResponse = 'לא נותרה יתרה בכרטיסיה של המשתמש';
                                    break;
                                default:
                                    alertResponse = '';
                            }
                            if (dataReponse == 0 || dataReponse == 1 || dataReponse == 2) {
                                var okAlert = _this.alertCtrl.create({
                                    title: alertResponse,
                                    buttons: ['אישור']
                                });
                                okAlert.present();
                                if (dataReponse == 0 || dataReponse == 1) {
                                    row.paid_count = 1;
                                    row.paid_method = 1;
                                }
                            }
                            else {
                                var Confirmalert2 = _this.alertCtrl.create({
                                    title: 'לא נותרה יתרה בכרטיסיה של המשתמש',
                                    buttons: [
                                        {
                                            text: 'קניית כרטיסיה ותשלום',
                                            handler: function () {
                                                _this.server.ClassPayment('ClassCashPaymentTicketBuy', _this.senddetails, localStorage.getItem("userid")).then(function (data) {
                                                    console.log("ClassCashPaymentTicketBuy : ", data.json());
                                                    var dataReponse = data.json();
                                                    var okAlert = _this.alertCtrl.create({
                                                        title: "קניית כרטיסיה ותשלום בוצע בהצלחה",
                                                        buttons: ['אישור']
                                                    });
                                                    okAlert.present();
                                                });
                                                row.paid_count = 1;
                                                row.paid_method = 1;
                                            }
                                        },
                                        {
                                            text: 'תשלום בחוב',
                                            handler: function () {
                                                _this.server.ClassPayment('ClassCashPaymentDebt', _this.senddetails, localStorage.getItem("userid")).then(function (data) {
                                                    console.log("ClassCashPaymentDebt : ", data.json());
                                                    var dataReponse = data.json();
                                                    var okAlert = _this.alertCtrl.create({
                                                        title: "תשלום בחוב בוצע בהצלחה",
                                                        buttons: ['אישור']
                                                    });
                                                    okAlert.present();
                                                });
                                                row.paid_count = 1;
                                                row.paid_method = 1;
                                            }
                                        },
                                        {
                                            text: 'ביטול',
                                            role: 'cancel',
                                            handler: function () {
                                                console.log('Cancel clicked');
                                            }
                                        },
                                    ]
                                });
                                Confirmalert2.present();
                            }
                        });
                    }
                },
                {
                    text: 'ביטול',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        alertConfirm.present();
    };
    TeacherconfirmclassPage.prototype.paymentDebt = function (row) {
        var _this = this;
        this.senddetails.user_id = row.user_id;
        this.senddetails.class_id = row.class_id;
        this.senddetails.payment_method = 2;
        var alertConfirm = this.alertCtrl.create({
            title: 'אישור רכישה',
            message: 'האם לאשר רכישה בחוב?',
            buttons: [
                {
                    text: 'אישור',
                    handler: function () {
                        _this.server.ClassPayment('ClassDebtPayment', _this.senddetails, localStorage.getItem("userid")).then(function (data) {
                            console.log("ClassCashPayment : ", data.json());
                            var dataReponse = data.json();
                            var alertResponse = '';
                            switch (dataReponse) {
                                case 0:
                                    alertResponse = 'רכישה בחוב בוצעה בהצלחה';
                                    break;
                                case 1:
                                    alertResponse = 'תשלום כבר בוצעה';
                                    break;
                                default:
                                    alertResponse = '';
                            }
                            var okAlert = _this.alertCtrl.create({
                                title: alertResponse,
                                buttons: ['אישור']
                            });
                            okAlert.present();
                            row.paid_count = 1;
                            row.paid_method = 2;
                        });
                    }
                },
                {
                    text: 'ביטול',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        alertConfirm.present();
    };
    TeacherconfirmclassPage.prototype.backButtonClick = function () {
        this.navCtrl.pop();
    };
    TeacherconfirmclassPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-teacherconfirmclass',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\ezmath\src\pages\teacherconfirmclass\teacherconfirmclass.html"*/'<!--\n  Generated template for the ClasssignupPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n\n\n\n<ion-content padding>\n   <!-- <back-button></back-button> -->\n\n\n  <div>\n    <div class="headerLeftIcon" (click)="backButtonClick()">\n      <ion-icon name="ios-arrow-back" style="margin-left: 6px;"></ion-icon>\n      <div>חזור</div>\n    </div>\n  </div>\n\n\n\n  <div class="mainLogo" align="center">\n    <img src="images/logo.png" style="width:50%; margin: auto">\n  </div>\n\n  <div class="mainTitle" >\n    <img src="images/subscribe.png" width="100%"/>\n  </div>\n\n  <div  *ngIf="!showFieldsdiv" (click)="showFieldsSearch()">\n    <ion-icon name="search" style="font-size:30px;"></ion-icon>\n  </div>\n\n  <div id="fieldsSearch" *ngIf="showFieldsdiv">\n    <div class="selectInfo">\n      <!--<select class="form-control" [(ngModel)]="info.professionselect">-->\n        <!--<option value="">בחירת מקצוע</option>-->\n        <!--<option [value]="row.id" *ngFor="let row of professionArray">{{row.title}}</option>-->\n      <!--</select>-->\n\n      <select class="form-control" [(ngModel)]="info.branchselect">\n        <option value="">בחירת סניף</option>\n        <option [value]="row.id" *ngFor="let row of brancesArray">{{row.title}}</option>\n      </select>\n\n\n      <input type="date" class="form-control" [(ngModel)]="info.class_date">\n      <button ion-button full class="regButton" (click)="searchClass()" >חיפוש</button>\n    </div>  </div>\n\n\n\n\n  <div *ngIf="searchArray.length > 0" class="MT20">\n\n    <div class="mainRow1" *ngFor="let row of searchArray let i=index">\n      <div class="rowDiv">\n        <div class="Title1">שם התלמיד/ה :  {{row.user_info.student_name}}</div>\n        <div class="Title1">שם השיעור :  {{row.class_title}}</div>\n        <div class="Title1">מקצוע :  {{row.professions}}</div>\n        <div class="Date1">\n          <button ion-button color="light" style="width: 100%" type="button">\n            <div (click)="confirmClass(row,2)" style="margin-left:20px;">\n              <ion-icon name="md-close"  [ngClass]="row.confirmed_status == 2 ? \'UnApprovedClass\' : \'\'"></ion-icon>\n            </div>\n\n            <div (click)="confirmClass(row,1)" [ngClass]="row.confirmed_status == 1 ? \'ApprovedClass\' : \'\'" style="">\n              <ion-icon name="md-checkmark" ></ion-icon>\n            </div>\n          </button>\n        </div>\n      </div>\n      <div class="rowDiv" align="center">\n\n        <div  *ngIf="row.paid_count == 0">\n          <ion-grid>\n          <ion-row justify-content-center>\n          <ion-col col-4>\n            <button ion-button color="light" style="width: 100%" type="button" (click)="paymentTicket(row)">כרטיסיה</button>\n          </ion-col>\n            <ion-col col-4>\n            <button ion-button color="light" style="width: 100%" type="button" (click)="paymentCash(row)">מזומן</button>\n            </ion-col>\n              <ion-col col-4>\n                <button ion-button color="light" style="width: 100%" type="button" (click)="paymentDebt(row)">חוב</button>\n              </ion-col>\n          </ion-row>\n          </ion-grid>\n      </div>\n\n\n        <div  *ngIf="row.paid_count == 1">\n          <ion-grid>\n            <ion-row justify-content-center>\n              <ion-col col-12>\n                <button ion-button color="secondary" style="width: 100%" type="button" *ngIf="row.paid_method == 0">שולם במזומן</button>\n                <button ion-button color="secondary" style="width: 100%" type="button" *ngIf="row.paid_method == 1">שולם בכרטיסיה</button>\n                <button ion-button color="secondary" style="width: 100%" type="button" *ngIf="row.paid_method == 2">שולם בחוב</button>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </div>\n\n\n        <div >\n          <ion-grid>\n            <ion-row justify-content-center>\n              <ion-col col-12>\n                <button ion-button color="light" style="width: 100%" [disabled]="!row.paid_count == 0" type="button" *ngIf="row.didnt_show == 0" (click)="didntShowClass(row,0)">לא הגיע</button>\n                <button ion-button color="secondary" style="width: 100%" [disabled]="!row.paid_count == 0" type="button" *ngIf="row.didnt_show == 1" (click)="didntShowClass(row,1)">לא הגיע</button>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </div>\n\n\n\n      </div>\n    </div>\n\n\n  </div>\n\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\USER\Desktop\github\ezmath\src\pages\teacherconfirmclass\teacherconfirmclass.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__services_server_service__["a" /* ServerService */],
            __WEBPACK_IMPORTED_MODULE_4__services_toast_service__["a" /* ToastService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], TeacherconfirmclassPage);
    return TeacherconfirmclassPage;
}());

//# sourceMappingURL=teacherconfirmclass.js.map

/***/ })

});
//# sourceMappingURL=0.js.map